import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

public class BuildFileEntities {

	public static ArrayList<FileEntity> build() {
		
			ArrayList<FileEntity> fEntities = new ArrayList<>();
			File folderTxtFilePaths = new File(InputParameters.txtInputFilePaths);
			File[] listOfFiles = folderTxtFilePaths.listFiles();

			for (File file : listOfFiles) {

				FileEntity fEntity = new FileEntity();

				try (Scanner sc = new Scanner(file, "UTF-8")) {

					fEntity.setId(UtilFunctions.fileNameWithOutExt(file.getName()));

					while (sc.hasNextLine()) {

						String lineT = sc.nextLine();
						String lineN = "";
						while (sc.hasNextLine()) {
							String line = sc.nextLine();
							if (line.startsWith("N") && StringUtils.substringBetween(line, "N", "	Reference")!=null) { // if (StringUtils.substringBetween(line, "N", "	Reference")!=null) 
								lineN = line;
								break;
							} else {
								lineT += ("" + line);
							}

						}

						///////// Parse Line T
						Scanner scTemp = new Scanner(lineT);
						scTemp.next();
						scTemp.next();
						String location1 = scTemp.next();
						String location2 = scTemp.next();
						String location = "[" + location1 + "--" + location2 + "]";

						String ontology_term = StringUtils.substringAfter(lineT, location2).trim();

						///////// Parse Line N
						scTemp = new Scanner(lineN);
						scTemp.next();
						scTemp.next();
						scTemp.next();
						//System.out.println(fEntity.getId());
						String taxonID = scTemp.next();
						// Remove URL from Taxon ID
						taxonID = StringUtils.substringAfter(taxonID, "http://purl.obolibrary.org/obo/NCBITaxon_");

						configureNewEntity(fEntity, ontology_term, taxonID, location);

					}
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				fEntities.add(fEntity);
			}

		
			
			return fEntities;
		}

		private static Entity configureNewEntity(FileEntity fEntity, String ontology_term, String taxonID,
				String location) {

			ArrayList<Entity> entities = fEntity.getEntities();
			for (int i = 0; i < entities.size(); i++) {
				Entity en = entities.get(i);
				if (InputParameters.Evaluation_Mode==EvaluationMode.BY_TAXONID && en.getTaxonID().equals(taxonID)) {
					en.getLocations().add(location);
					en.getOntology_term().add(ontology_term);
					return en;
				}
				else if (InputParameters.Evaluation_Mode==EvaluationMode.BY_ONTOLOGY && en.getOntology_term().get(0).equals(ontology_term)) {
					en.getLocations().add(location);
					return en;
				}
			}

			Entity en = new Entity();
			en.getOntology_term().add(ontology_term);
			en.setTaxonID(taxonID);
			en.getLocations().add(location);

			fEntity.getEntities().add(en);

			return en;
		}
	
	
}
