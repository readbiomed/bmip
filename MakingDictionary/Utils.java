import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class Utils {

	static int numberOfExactMatches = 0;

	public static String findSameItemInAxiomIfExists(List<String> searchItemsList,
			ArrayList<String> rawAxiomStringsforCurrentClass) {

		for (int i = 0; i < rawAxiomStringsforCurrentClass.size(); i++) {
			String searchItem = StringUtils.substringBetween(rawAxiomStringsforCurrentClass.get(i),
					"<owl:annotatedTarget rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">",
					"</owl:annotatedTarget>");
			if (searchItemsList.contains(searchItem))
				return searchItem;

		}

		return null;

	}

	public static boolean findPartialMatchItem_class(List<String> searchItemsList, String stringToSearch_class) {

		boolean found = false;
		for (int i = 0; i < searchItemsList.size(); i++) {
			String searchItem = searchItemsList.get(i);
			if (stringToSearch_class.startsWith(searchItem + " ")
					|| stringToSearch_class.contains(" " + searchItem + " ")
					|| stringToSearch_class.endsWith(" " + searchItem) || stringToSearch_class.equals(searchItem)) {
				if (stringToSearch_class.equals(searchItem)) {
					numberOfExactMatches++;
				}
				return true;
			}
		}

		return false;

	}

	public static boolean findPartialMatchItem_axiom(List<String> searchItemsList,
			ArrayList<String> rawAxiomStringsforCurrentClass) {

		for (int i = 0; i < rawAxiomStringsforCurrentClass.size(); i++) {
			String searchItem_axiom = StringUtils.substringBetween(rawAxiomStringsforCurrentClass.get(i),
					"<owl:annotatedTarget rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">",
					"</owl:annotatedTarget>");

			for (int j = 0; j < searchItemsList.size(); j++) {
				String searchItem = searchItemsList.get(j);
				if (searchItem_axiom.startsWith(searchItem + " ") || searchItem_axiom.contains(" " + searchItem + " ")
						|| searchItem_axiom.endsWith(" " + searchItem) || searchItem_axiom.equals(searchItem)) {
					if (searchItem_axiom.equals(searchItem)) {
						numberOfExactMatches++;
					}
					return true;
				}
			}

		}
		return false;
	}

	public static ArrayList<String> findUnfoundSearchItems(List<String> searchItemsList,
			ArrayList<OwlClass> owlClassesResuts) {

		ArrayList<String> unFoundItems = new ArrayList<>(searchItemsList);

		for (int i = 0; i < searchItemsList.size(); i++) {
			String searchItem = searchItemsList.get(i);

			outerloop: for (int j = 0; j < owlClassesResuts.size(); j++) {
				OwlClass owlclass = owlClassesResuts.get(j);
				if (owlclass.getCanonical().contains(" " + searchItem + " ")
						|| owlclass.getCanonical().startsWith(searchItem + " ")
						|| owlclass.getCanonical().endsWith(" " + searchItem)
						|| owlclass.getCanonical().equals(searchItem)) {
					unFoundItems.remove(searchItem);
					break outerloop;
				}
				for (int j2 = 0; j2 < owlclass.getVariants().size(); j2++) {
					String variant = owlclass.getVariants().get(j2);
					if (variant.contains(" " + searchItem + " ") || variant.startsWith(searchItem + " ")
							|| variant.endsWith(" " + searchItem) || variant.equals(searchItem)) {
						unFoundItems.remove(searchItem);
						break outerloop;
					}
				}
			}

		}

		return unFoundItems;
	}

	public static void addStopWordAxiom(OwlClass owlclass) {

		ArrayList<String> stopWords = StopWordsFileReader.read(InputParameters.StopWordsFileName);
		for (int i = 0; i < stopWords.size(); i++) {
			String stopWord = stopWords.get(i);
			String newVariant = "";
			if (owlclass.getCanonical().endsWith(" " + stopWord)) {
				newVariant = StringUtils.substringBefore(owlclass.getCanonical(), " " + stopWord);
				if (!owlclass.getVariants().contains(newVariant))
					owlclass.getVariants().add(newVariant);
				break;
			}
			if (owlclass.getCanonical().contains(" " + stopWord + " ")) {
				newVariant = owlclass.getCanonical().replace(stopWord + " ", "");
				if (!owlclass.getVariants().contains(newVariant))
					owlclass.getVariants().add(newVariant);
				break;
			}
			if (owlclass.getCanonical().startsWith(stopWord + " ")) {
				newVariant = StringUtils.substringBefore(owlclass.getCanonical(), stopWord + " ");
				if (!owlclass.getVariants().contains(newVariant))
					owlclass.getVariants().add(newVariant);
				break;
			}

		}

	}

	public static boolean hasSameOwlClass(OwlClass owlclass, ArrayList<OwlClass> owlClassesResuts) {
		for (int i = 0; i < owlClassesResuts.size(); i++) {
			if (owlClassesResuts.get(i).getId().equals(owlclass.getId()))
				return true;
		}
		return false;
	}

}
