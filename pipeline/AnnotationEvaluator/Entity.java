

import java.util.ArrayList;

public class Entity {

	String taxonID;
//	String ontology_term;
	ArrayList<String> locations;
	ArrayList<String> ontology_term;
	
	public Entity() {
		locations = new ArrayList<>();
		ontology_term = new ArrayList<>();
	}

	public String getTaxonID() {
		return taxonID;
	}

	public void setTaxonID(String taxonID) {
		this.taxonID = taxonID;
	}

	

	public ArrayList<String> getOntology_term() {
		return ontology_term;
	}

	public void setOntology_term(ArrayList<String> ontology_term) {
		this.ontology_term = ontology_term;
	}

	public ArrayList<String> getLocations() {
		return locations;
	}

	public void setLocations(ArrayList<String> locations) {
		this.locations = locations;
	}
	
	
	
	
}
