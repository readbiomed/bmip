
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

public class Test {

	public static void main(String[] args) {

		
		try (Scanner sc = new Scanner(new File(InputParameters.OWLFileName), "UTF-8")) {

			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				/////// If a new owl class is found ///////////
				if (line.contains("<owl:Class")) {
					String owlClassString = line + "\n";
					while (sc.hasNextLine()) {
						String classLine = sc.nextLine();
						owlClassString += classLine + "\n";
						if (classLine.endsWith("</owl:Class>")) {
							break;
						}
					} // Raw String of owl class is built
					
					// Checking whether the owl class is subclass of our search item or not

				
					String stringToSearch_class =StringUtils.substringBetween(owlClassString,
							"<rdfs:label rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">",
							"</rdfs:label>");		
					// check cloned_searchItemsList null or not
					if (stringToSearch_class.equals("Silvanigrella sp.")) {
						System.out.println(StringUtils.substringBetween(owlClassString, "rdf:about=\"", "\""));
					}
					// Owl class is not a subclass
				}
				
			}

			if (sc.ioException() != null) {
				try {
					throw sc.ioException();
				} catch (IOException e) {

					e.printStackTrace();
				}
			}
		} catch (FileNotFoundException e1) {

			e1.printStackTrace();
		};
	}

}
