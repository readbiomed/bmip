
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

public class Main {

	public static void main(String[] args) {
		DeletePreviousFiles.delete(InputParameters.TXTOutputFolder);

		ArrayList<String> searchItems = new ArrayList<>();
		searchItems.add("ArticleTitle");
		searchItems.add("AbstractText");

		File folderTxtFilePaths = new File(InputParameters.XMLInputFolder);
		File[] listOfFiles = folderTxtFilePaths.listFiles();

		for (File file : listOfFiles) {
			HashMap<String, String> data = ReadInputFile.read(file, searchItems);
			WriteOutputFile.write(file.getName(), data);
		}
	}
}
