
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

public class ConvertLabelsToTaxonIDs {

	
	public static List<String> convert (List<String> searchItemsList) {
		
		List<String> searchItemsList_TaxonIDs = new ArrayList<>();
		
		List<String> searchItemsList_Temp = new ArrayList<>(searchItemsList);
		
		
		try (Scanner sc = new Scanner(new File(InputParameters.OWLFileName), "UTF-8")) {

			while (sc.hasNextLine() && !searchItemsList_Temp.isEmpty()) {
				String line = sc.nextLine();
				/////// If a new owl class is found ///////////
				if (line.contains("<owl:Class")) {
					String owlClassString = line + "\n";
					while (sc.hasNextLine()) {
						String classLine = sc.nextLine();
						owlClassString += classLine + "\n";
						if (classLine.endsWith("</owl:Class>")) {
							break;
						}
					} // Raw String of owl class is built

					
					// Finding Potential Axioms After Class
					ArrayList<String> rawAxiomStringsforCurrentClass = new ArrayList<>();
					while (sc.hasNextLine()) {
						line = sc.nextLine();
						if (line.contains("<owl:Axiom>")) {
							String axiomString = line + "\n";
							while (sc.hasNextLine()) {
								String axiomLine = sc.nextLine();
								axiomString += axiomLine + "\n";
								if (axiomLine.endsWith("</owl:Axiom>")) {
									rawAxiomStringsforCurrentClass.add(axiomString);
									break;
								}
							}
						} else
							break;
					} // End Finding Potential Axioms After Class
					
					
					// Checking whether the owl class is our search item or not
					String stringToSearch_Axiom = "";
					String stringToSearch_class = "";
					
					//stringToSearch_Axiom = Utils.findSameItemIfExists(searchItemsList_Temp, rawAxiomStringsforCurrentClass); // return null if not exist
					stringToSearch_class =StringUtils.substringBetween(owlClassString,
								"<rdfs:label rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">",
								"</rdfs:label>");
					
					if (Utils.findPartialMatchItem_class(searchItemsList_Temp, stringToSearch_class) || 
							Utils.findPartialMatchItem_axiom(searchItemsList_Temp, rawAxiomStringsforCurrentClass)) {	
						searchItemsList_TaxonIDs.add(StringUtils.substringBetween(owlClassString, "rdf:about=\"", "\""));
							
//						System.out.println("************************************");
//						System.out.println(Utils.findPartialMatchItem_class(searchItemsList_Temp, stringToSearch_class));
//						System.out.println(Utils.findPartialMatchItem_axiom(searchItemsList_Temp, rawAxiomStringsforCurrentClass));
//						System.out.println(owlClassString);
					}	
				}	
			}

			if (sc.ioException() != null) {
				try {
					throw sc.ioException();
				} catch (IOException e) {

					e.printStackTrace();
				}
			}
		} catch (FileNotFoundException e1) {

			e1.printStackTrace();
		}
		
		
		
		return searchItemsList_TaxonIDs;
	}
	
	

	
	
	

	
}
