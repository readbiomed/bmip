<?xml version="1.0" ?>
<!DOCTYPE PubmedArticleSet PUBLIC "-//NLM//DTD PubMedArticle, 1st January 2019//EN" "https://dtd.nlm.nih.gov/ncbi/pubmed/out/pubmed_190101.dtd">
<PubmedArticleSet>
<PubmedArticle>
    <MedlineCitation Status="MEDLINE" Owner="NLM">
        <PMID Version="1">27075398</PMID>
        <DateCompleted>
            <Year>2018</Year>
            <Month>02</Month>
            <Day>02</Day>
        </DateCompleted>
        <DateRevised>
            <Year>2018</Year>
            <Month>03</Month>
            <Day>13</Day>
        </DateRevised>
        <Article PubModel="Print-Electronic">
            <Journal>
                <ISSN IssnType="Electronic">1096-9101</ISSN>
                <JournalIssue CitedMedium="Internet">
                    <Volume>48</Volume>
                    <Issue>6</Issue>
                    <PubDate>
                        <Year>2016</Year>
                        <Month>08</Month>
                    </PubDate>
                </JournalIssue>
                <Title>Lasers in surgery and medicine</Title>
                <ISOAbbreviation>Lasers Surg Med</ISOAbbreviation>
            </Journal>
            <ArticleTitle>Investigating skin penetration depth and shape following needle-free injection at different pressures: A cadaveric study.</ArticleTitle>
            <Pagination>
                <MedlinePgn>624-8</MedlinePgn>
            </Pagination>
            <ELocationID EIdType="doi" ValidYN="Y">10.1002/lsm.22517</ELocationID>
            <Abstract>
                <AbstractText Label="BACKGROUND AND OBJECTIVES">The effectiveness of needle-free injection devices in neocollagenesis for treating extended skin planes is an area of active research. It is anticipated that needle-free injection systems will not only be used to inject vaccines or insulin, but will also greatly aid skin rejuvenation when used to inject aesthetic materials such as hyaluronic acid, botulinum toxin, and placental extracts. There has not been any specific research to date examining how materials penetrate the skin when a needle-free injection device is used. In this study, we investigated how material infiltrates the skin when it is injected into a cadaver using a needle-free device.</AbstractText>
                <AbstractText Label="STUDY DESIGN/MATERIALS AND METHODS">Using a needle-free injector (INNOJECTOR™; Amore Pacific, Seoul, Korea), 0.2 ml of 5% methylene blue (MB) or latex was injected into cheeks of human cadavers. The device has a nozzle diameter of 100 µm and produces a jet with velocity of 180 m/s. This jet penetrates the skin and delivers medicine intradermally via liquid propelled by compressed gasses. Materials were injected at pressures of 6 or 8.5 bars, and the injection areas were excised after the procedure. The excised areas were observed visually and with a phototrichogram to investigate the size, infiltration depth, and shape of the hole created on the skin. A small part of the area that was excised was magnified and stained with H&amp;E (×40) for histological examination.</AbstractText>
                <AbstractText Label="RESULTS">We characterized the shape, size, and depth of skin infiltration following injection of 5% MB or latex into cadaver cheeks using a needle-free injection device at various pressure settings. Under visual inspection, the injection at 6 bars created semi-circle-shaped hole that penetrated half the depth of the excised tissue, while injection at 8.5 bars created a cylinder-shaped hole that spanned the entire depth of the excised tissue. More specific measurements were collected using phototrichogram imaging. The shape of the injection entry point was consistently spherical regardless of the amount of pressure used. When injecting 5% MB at 6 bars, the depth of infiltration reached 2.323 mm, while that at 8.5 bars reached 8.906 mm. The area of the hole created by the 5% MB injection was 0.797 mm(2) at 6 bars and 0.242 mm(2) at 8.5 bars. Latex injections reached a depth of 3.480 mm at 6 bars and 7.558 mm at 8.5 bars, and the areas were measured at 1.043 mm(2) (6 bars) and 0.355 mm(2) (8.5 bars). Histological examination showed that the injection penetrated as deep as the superficial musculoaponeurotic system at 6 bars and the masseter muscle at 8.5 bars.</AbstractText>
                <AbstractText Label="CONCLUSION">When injecting material into the skin using a pneumatic needle-free injector, higher-pressure injections result in a hole with smaller area than lower-pressure injections. The depth and shape of skin penetration vary according to the amount of pressure applied. For materials of low density and viscosity, there is a greater difference in penetration depth according to the degree of pressure. Lasers Surg. Med. 48:624-628, 2016. © 2016 Wiley Periodicals, Inc.</AbstractText>
                <CopyrightInformation>© 2016 Wiley Periodicals, Inc.</CopyrightInformation>
            </Abstract>
            <AuthorList CompleteYN="Y">
                <Author ValidYN="Y">
                    <LastName>Seok</LastName>
                    <ForeName>Joon</ForeName>
                    <Initials>J</Initials>
                    <AffiliationInfo>
                        <Affiliation>Department of Dermatology, Chung-Ang University College of Medicine, Seoul, Korea.</Affiliation>
                    </AffiliationInfo>
                </Author>
                <Author ValidYN="Y">
                    <LastName>Oh</LastName>
                    <ForeName>Chang Taek</ForeName>
                    <Initials>CT</Initials>
                    <AffiliationInfo>
                        <Affiliation>Department of Medicine, Graduate School, Chung-Ang University, Seoul, South Korea.</Affiliation>
                    </AffiliationInfo>
                </Author>
                <Author ValidYN="Y">
                    <LastName>Kwon</LastName>
                    <ForeName>Hyun Jung</ForeName>
                    <Initials>HJ</Initials>
                    <AffiliationInfo>
                        <Affiliation>Department of Dermatology, Chung-Ang University College of Medicine, Seoul, Korea.</Affiliation>
                    </AffiliationInfo>
                </Author>
                <Author ValidYN="Y">
                    <LastName>Kwon</LastName>
                    <ForeName>Tae Rin</ForeName>
                    <Initials>TR</Initials>
                    <AffiliationInfo>
                        <Affiliation>Department of Medicine, Graduate School, Chung-Ang University, Seoul, South Korea.</Affiliation>
                    </AffiliationInfo>
                </Author>
                <Author ValidYN="Y">
                    <LastName>Choi</LastName>
                    <ForeName>Eun Ja</ForeName>
                    <Initials>EJ</Initials>
                    <AffiliationInfo>
                        <Affiliation>Department of Medicine, Graduate School, Chung-Ang University, Seoul, South Korea.</Affiliation>
                    </AffiliationInfo>
                </Author>
                <Author ValidYN="Y">
                    <LastName>Choi</LastName>
                    <ForeName>Sun Young</ForeName>
                    <Initials>SY</Initials>
                    <AffiliationInfo>
                        <Affiliation>Department of Dermatology, Chung-Ang University College of Medicine, Seoul, Korea.</Affiliation>
                    </AffiliationInfo>
                </Author>
                <Author ValidYN="Y">
                    <LastName>Mun</LastName>
                    <ForeName>Seog Kyun</ForeName>
                    <Initials>SK</Initials>
                    <AffiliationInfo>
                        <Affiliation>Department of Otorhinolaryngology-Head and Neck Surgery, Chung-Ang University College of Medicine, Seoul, Korea.</Affiliation>
                    </AffiliationInfo>
                </Author>
                <Author ValidYN="Y">
                    <LastName>Han</LastName>
                    <ForeName>Seung-Ho</ForeName>
                    <Initials>SH</Initials>
                    <AffiliationInfo>
                        <Affiliation>Department of Anatomy, Chung-Ang University College of Medicine, Seoul, Korea.</Affiliation>
                    </AffiliationInfo>
                </Author>
                <Author ValidYN="Y">
                    <LastName>Kim</LastName>
                    <ForeName>Beom Joon</ForeName>
                    <Initials>BJ</Initials>
                    <AffiliationInfo>
                        <Affiliation>Department of Dermatology, Chung-Ang University College of Medicine, Seoul, Korea.</Affiliation>
                    </AffiliationInfo>
                </Author>
                <Author ValidYN="Y">
                    <LastName>Kim</LastName>
                    <ForeName>Myeung Nam</ForeName>
                    <Initials>MN</Initials>
                    <AffiliationInfo>
                        <Affiliation>Department of Dermatology, Chung-Ang University College of Medicine, Seoul, Korea.</Affiliation>
                    </AffiliationInfo>
                </Author>
            </AuthorList>
            <Language>eng</Language>
            <PublicationTypeList>
                <PublicationType UI="D016428">Journal Article</PublicationType>
                <PublicationType UI="D013485">Research Support, Non-U.S. Gov't</PublicationType>
            </PublicationTypeList>
            <ArticleDate DateType="Electronic">
                <Year>2016</Year>
                <Month>04</Month>
                <Day>13</Day>
            </ArticleDate>
        </Article>
        <MedlineJournalInfo>
            <Country>United States</Country>
            <MedlineTA>Lasers Surg Med</MedlineTA>
            <NlmUniqueID>8007168</NlmUniqueID>
            <ISSNLinking>0196-8092</ISSNLinking>
        </MedlineJournalInfo>
        <ChemicalList>
            <Chemical>
                <RegistryNumber>0</RegistryNumber>
                <NameOfSubstance UI="D007840">Latex</NameOfSubstance>
            </Chemical>
            <Chemical>
                <RegistryNumber>T42P99266K</RegistryNumber>
                <NameOfSubstance UI="D008751">Methylene Blue</NameOfSubstance>
            </Chemical>
        </ChemicalList>
        <CitationSubset>IM</CitationSubset>
        <MeshHeadingList>
            <MeshHeading>
                <DescriptorName UI="D002610" MajorTopicYN="N">Cheek</DescriptorName>
            </MeshHeading>
            <MeshHeading>
                <DescriptorName UI="D006801" MajorTopicYN="N">Humans</DescriptorName>
            </MeshHeading>
            <MeshHeading>
                <DescriptorName UI="D007277" MajorTopicYN="N">Injections, Jet</DescriptorName>
            </MeshHeading>
            <MeshHeading>
                <DescriptorName UI="D007840" MajorTopicYN="N">Latex</DescriptorName>
                <QualifierName UI="Q000008" MajorTopicYN="Y">administration &amp; dosage</QualifierName>
                <QualifierName UI="Q000493" MajorTopicYN="N">pharmacokinetics</QualifierName>
            </MeshHeading>
            <MeshHeading>
                <DescriptorName UI="D008751" MajorTopicYN="N">Methylene Blue</DescriptorName>
                <QualifierName UI="Q000008" MajorTopicYN="Y">administration &amp; dosage</QualifierName>
                <QualifierName UI="Q000493" MajorTopicYN="N">pharmacokinetics</QualifierName>
            </MeshHeading>
            <MeshHeading>
                <DescriptorName UI="D011312" MajorTopicYN="N">Pressure</DescriptorName>
            </MeshHeading>
            <MeshHeading>
                <DescriptorName UI="D012867" MajorTopicYN="N">Skin</DescriptorName>
                <QualifierName UI="Q000737" MajorTopicYN="Y">chemistry</QualifierName>
                <QualifierName UI="Q000473" MajorTopicYN="N">pathology</QualifierName>
            </MeshHeading>
        </MeshHeadingList>
        <KeywordList Owner="NOTNLM">
            <Keyword MajorTopicYN="Y">cadaver</Keyword>
            <Keyword MajorTopicYN="Y">depth</Keyword>
            <Keyword MajorTopicYN="Y">needle-free injector</Keyword>
            <Keyword MajorTopicYN="Y">needleless microjet device</Keyword>
            <Keyword MajorTopicYN="Y">penetration</Keyword>
            <Keyword MajorTopicYN="Y">pressure</Keyword>
            <Keyword MajorTopicYN="Y">shape</Keyword>
        </KeywordList>
    </MedlineCitation>
    <PubmedData>
        <History>
            <PubMedPubDate PubStatus="accepted">
                <Year>2016</Year>
                <Month>03</Month>
                <Day>20</Day>
            </PubMedPubDate>
            <PubMedPubDate PubStatus="entrez">
                <Year>2016</Year>
                <Month>4</Month>
                <Day>15</Day>
                <Hour>6</Hour>
                <Minute>0</Minute>
            </PubMedPubDate>
            <PubMedPubDate PubStatus="pubmed">
                <Year>2016</Year>
                <Month>4</Month>
                <Day>15</Day>
                <Hour>6</Hour>
                <Minute>0</Minute>
            </PubMedPubDate>
            <PubMedPubDate PubStatus="medline">
                <Year>2018</Year>
                <Month>2</Month>
                <Day>3</Day>
                <Hour>6</Hour>
                <Minute>0</Minute>
            </PubMedPubDate>
        </History>
        <PublicationStatus>ppublish</PublicationStatus>
        <ArticleIdList>
            <ArticleId IdType="pubmed">27075398</ArticleId>
            <ArticleId IdType="doi">10.1002/lsm.22517</ArticleId>
        </ArticleIdList>
    </PubmedData>
</PubmedArticle>

</PubmedArticleSet>