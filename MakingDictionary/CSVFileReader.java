

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class CSVFileReader {

	static String URLPrefix = "http://purl.obolibrary.org/obo/NCBITaxon_";
	
	static int Canonical_Column = 0;
	static int TAXONID_Column = 1;

	public static ArrayList<String> read (String fileName) {

		int item_Column = -1;
		if (InputParameters.searchMode == SearchMode.BY_LABEL )
			item_Column = Canonical_Column;
		else if (InputParameters.searchMode == SearchMode.BY_TAXONID)
			item_Column = TAXONID_Column;
		
		
		ArrayList<String> searchItemsList = new ArrayList<>();
		
		//// There is an Unknown Exception !!!!!!!!!!!!!
		boolean firstLineErrorRemover = false;
		////////////////////////////////
		try (Scanner sc = new Scanner(new File(fileName), "UTF-8")) {
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				String item = line.split(",")[item_Column];
				//// There is an Unknown Exception !!!!!!!!!!!!!
				if (firstLineErrorRemover && InputParameters.searchMode == SearchMode.BY_LABEL) {
					item=item.substring(1);
					firstLineErrorRemover = false;
				}
				////////////////////////////////
				
				// Concat URL to TAXON ID
				if(InputParameters.searchMode == SearchMode.BY_TAXONID && !item.startsWith(URLPrefix)) {
					item = URLPrefix + item;
				}
				////////////////////////////////////
				
				searchItemsList.add(item);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
		
		
	       
	   // Remove Similar Items in List     
		Set<String> s = new LinkedHashSet<>(searchItemsList);
		searchItemsList.clear();
		searchItemsList.addAll(s);
		////////////////////////////
		
	
	        
	        return searchItemsList;
	        
	    }

}
