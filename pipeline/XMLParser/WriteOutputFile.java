import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;



public class WriteOutputFile {

	private static PrintWriter printWriter;

	public static void write(String fileName, HashMap<String, String> data) {
		File file = new File(InputParameters.TXTOutputFolder);
		file.mkdirs();
		FileWriter fileWriter;
		try {
			fileWriter = new FileWriter(
					InputParameters.TXTOutputFolder + "/" + UtilFunctions.fileNameWithOutExt(fileName) + ".txt");
			printWriter = new PrintWriter(fileWriter);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		printWriter.println();
		
		
//		for (String key : data.keySet()) {
//
//			String[] dataArr = data.get(key).split("[.] [A-Z]");
//			for (int i = 0; i < dataArr.length; i++) {
//					printWriter.println( dataArr[i]);
//			}
//			printWriter.println();
//		}

		 for (String key : data.keySet())
		 {
			 BreakIterator bi = BreakIterator.getSentenceInstance();	
		     bi.setText(data.get(key));
		     int index = 0;
			 while (bi.next() != BreakIterator.DONE) {
					String sentence = data.get(key).substring(index, bi.current());		
					index = bi.current();
					printWriter.println(sentence);
					}
		 printWriter.println();
		 }

		printWriter.close();
		
		
		
		
//		String paragraph = "Although a Yersinia pseudotuberculosis (Yptb) lung infection model has been developed to study Y. pestis pathogenesis, it is still necessary to establish a new animal model to mimic the pathophysiological features induced by Y. pestis infection. Here, we provide a new lung infection model using the Yptb strain, IP2777, which displayed rapid spread of bacteria to the liver, spleen, and blood.";
//		BreakIterator bi = BreakIterator.getSentenceInstance();
//		
//		bi.setText(paragraph);
//	
//		int index = 0;
//	
//		while (bi.next() != BreakIterator.DONE) {
//		
//		String sentence = paragraph.substring(index, bi.current());
//		
//		System.out.println("Sentence: " + sentence);
//		
//		index = bi.current();
//		
//		}

	}

}
