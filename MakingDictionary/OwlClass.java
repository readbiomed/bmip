
import java.util.ArrayList;

public class OwlClass {

	
	private String id;
	private String canonical;
	private ArrayList<String> variants;
	
	
	private String rawOwlClassString;
	private ArrayList<String> rawAxiomStrings;
	
	public OwlClass() {
		rawAxiomStrings = new ArrayList<>();
		variants = new ArrayList<>();
	}
	
	
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCanonical() {
		return canonical;
	}
	public void setCanonical(String canonical) {
		this.canonical = canonical;
	}
	public ArrayList<String> getVariants() {
		return variants;
	}
	public void setVariants(ArrayList<String> variants) {
		this.variants = variants;
	}




	public String getRawOwlClassString() {
		return rawOwlClassString;
	}




	public void setRawOwlClassString(String rawOwlClassString) {
		this.rawOwlClassString = rawOwlClassString;
	}




	public ArrayList<String> getRawAxiomStrings() {
		return rawAxiomStrings;
	}




	public void setRawAxiomStrings(ArrayList<String> rawAxiomStrings) {
		this.rawAxiomStrings = rawAxiomStrings;
	}
	
	
	
	
}
