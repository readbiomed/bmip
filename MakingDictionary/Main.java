
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

public class Main {



	public static void main(String[] args) {

		List<String> searchItemsList = CSVFileReader.read(InputParameters.CSVFileName);

		
		
		
		ArrayList<OwlClass> owlClassesResuts = new ArrayList<>();

		
		
		try (Scanner sc = new Scanner(new File(InputParameters.OWLFileName), "UTF-8")) {

			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				/////// If a new owl class is found ///////////
				if (line.contains("<owl:Class")) {
					String owlClassString = line + "\n";
					while (sc.hasNextLine()) {
						String classLine = sc.nextLine();
						owlClassString += classLine + "\n";
						if (classLine.endsWith("</owl:Class>")) {
							break;
						}
					} // Raw String of owl class is built

					
					// Finding Potential Axioms After Class
					ArrayList<String> rawAxiomStringsforCurrentClass = new ArrayList<>();
					while (sc.hasNextLine()) {
						line = sc.nextLine();
						if (line.contains("<owl:Axiom>")) {
							String axiomString = line + "\n";
							while (sc.hasNextLine()) {
								String axiomLine = sc.nextLine();
								axiomString += axiomLine + "\n";
								if (axiomLine.endsWith("</owl:Axiom>")) {
									rawAxiomStringsforCurrentClass.add(axiomString);
									break;
								}
							}
						} else
							break;
					} // End Finding Potential Axioms After Class
					
					
					
					// Checking whether the owl class is our search item or not (by checking class name, axioms names and subclass field)
					String stringToSearch_class = "";
					if (InputParameters.searchMode == SearchMode.BY_LABEL) {
						stringToSearch_class =StringUtils.substringBetween(owlClassString,
								"<rdfs:label rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">",
								"</rdfs:label>");		
					}
					if (InputParameters.searchMode == SearchMode.BY_TAXONID)
						stringToSearch_class = StringUtils.substringBetween(owlClassString, "rdf:about=\"", "\"");	
					
					// check cloned_searchItemsList null or not
					if (Utils.findPartialMatchItem_class(searchItemsList, stringToSearch_class)
						||	Utils.findPartialMatchItem_axiom(searchItemsList, rawAxiomStringsforCurrentClass)) {
						OwlClass owlclass = new OwlClass();
						owlclass.setRawOwlClassString(owlClassString);
						owlclass.setId(StringUtils.substringBetween(owlClassString, "rdf:about=\"", "\""));
						owlclass.setCanonical(StringUtils.substringBetween(owlClassString,
								"<rdfs:label rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">",
								"</rdfs:label>"));
				
						// Add Axiom Information to Owl Class Object
						for (int i = 0; i < rawAxiomStringsforCurrentClass.size(); i++) {
							String axiomString = rawAxiomStringsforCurrentClass.get(i);
							owlclass.getRawAxiomStrings().add(axiomString);
							owlclass.getVariants().add(StringUtils.substringBetween(axiomString,
									"<owl:annotatedTarget rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">",
									"</owl:annotatedTarget>"));
						}
						////////////// Axiom by Stop Words//////////////
							Utils.addStopWordAxiom(owlclass);
						/////////////////////////////////////////////////////////////////

						owlClassesResuts.add(owlclass);
					}
					// Owl class is not our search Item (non of three conditions is satisfied)
				}
				
			}

			if (sc.ioException() != null) {
				try {
					throw sc.ioException();
				} catch (IOException e) {

					e.printStackTrace();
				}
			}
		} catch (FileNotFoundException e1) {

			e1.printStackTrace();
		}

		////////////////// Find Unfound Search Items in Owl File////////////////////
		ArrayList<String> unFoundSearchItems = Utils.findUnfoundSearchItems(searchItemsList, owlClassesResuts);
		//////////////////////////////////////////////////////////////////////////////////////////////
		
		/////////////////////////// Print number of exact matches ////////////////////////////////////////
		System.out.println("Number of Exact Matches: "+Utils.numberOfExactMatches);
		/////////////////////////////////////////////////////////////////////////////////////////////////
		
		//////////////////////// Find All level subclasses in Owl file///////////////////////////////

		List<String> searchItemsList_TaxonIDs_CurrentLevel = null;
		if (InputParameters.searchMode == SearchMode.BY_LABEL)
			searchItemsList_TaxonIDs_CurrentLevel = ConvertLabelsToTaxonIDs.convert(searchItemsList);
		else if (InputParameters.searchMode == SearchMode.BY_TAXONID)
			searchItemsList_TaxonIDs_CurrentLevel = new ArrayList<String>(searchItemsList);
		
		FindAllLevelSubclasses.traverse(searchItemsList_TaxonIDs_CurrentLevel, owlClassesResuts);
		
		///////////////////////////////////////////////////////////////////////////////////////////////
		
		
		
		//////////////////////////////Writing Dictionary XML File
		XMLFileWriter writer = new XMLFileWriter(InputParameters.XMLFileName);
		writer.beginWriting();
		for (int j = 0; j < owlClassesResuts.size(); j++) {
			OwlClass owlclass = owlClassesResuts.get(j);
			writer.writeFoundToken(owlclass.getId(), owlclass.getCanonical(), owlclass.getVariants());
		}
		for (int j = 0; j < unFoundSearchItems.size(); j++) {
			writer.appendNotFoundToken(unFoundSearchItems.get(j));
		}
		writer.finishWriting();
		
///////////////////////////////////////////////////////////////////////////////////////////////////////////////		
		
		/////////// Write MetaMap input files  for phase 2////////////////////
		NotFoundInputFileWriter notFoundWriter= new NotFoundInputFileWriter(InputParameters.NotFoundItemsFileName);
		notFoundWriter.write(unFoundSearchItems);
		notFoundWriter.finishWriting();
		////////////////////////////////////////////////////////////////////////
		
	
	}

}
