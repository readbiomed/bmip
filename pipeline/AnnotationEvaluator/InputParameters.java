
public class InputParameters {

	public static String txtInputFilePaths = "../output-files-annotator/";
	
	public static String exlPMIDFilesFolder = "Excel PMID Files";
	
	public static String exlEvalFilesFolder = "Excel Evaluation Files";
	
	public static String labelFilesFolder = "Label Files";
	
	
	public static String exlLabelEvaluationFileOutputName = "Label Evaluation.xlsx";
//	public static String exlLabelEvaluationFileOutputName_FP = "Label Evaluation FP.xlsx";
//	public static String exlLabelEvaluationFileOutputName_TP = "Label Evaluation TP.xlsx";
//	public static String exlLabelEvaluationFileOutputName_Total = "Label Evaluation Total.xlsx";
	
	public static String exlMainEvaluationFileOutputName = "evaluation.xlsx";
	
	public static String exlReferenceFileInput = "Workbook2.xlsx";
	
	public static String exlNumberReportFileOutput = "NumberReportFile.xlsx";
	
	
	
	
	
	public static int Evaluation_Mode = EvaluationMode.BY_TAXONID;
	
	
	
	public static String exlMergedFilesOutputName = "merged.xlsx";
	
}
