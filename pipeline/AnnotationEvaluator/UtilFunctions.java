
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

public class UtilFunctions {

	public static String fileNameWithOutExt(String fileName) {
		return Optional.of(fileName.lastIndexOf(".")).filter(i -> i >= 0).map(i -> fileName.substring(0, i))
				.orElse(fileName);
	}

}
