import java.io.File;
import java.util.ArrayList;

public class DeletePreviousFiles {

	public static void delete(String path) {
		File exlFolder = new File(path);
		if (exlFolder.exists()) {
			String[] entries = exlFolder.list();
			for (String s : entries) {
				File currentFile = new File(exlFolder.getPath(), s);
				currentFile.delete();
			}
		}
		exlFolder.delete();
	}
	
}
