# BMIP Pipeline

## Introduction

There are two folders in the BMIP directory.
First one is “Make dictionary” which is used for generating targeted dictionary based on BMIP list, and the second folder “Pipeline” is used for annotating the texts and assigning labels to the annotated pathogens based on SciDT method.


## Installation

In order to install all required packages and git-cloning two modules of "nlp-pipelines-conceptmapper" and " ScientificDiscourseTagging-master",  run "installation" file in the "Pipeline" folder.

./installation

## Making dictionary

Input: BMIPlist” MMandNCBI.txt”, list of stop words “stopwords.txt”, NCBI Owl file”NCBItaxon.Owl”.

Output: results.XML


Put the input files in the “Making dictionary” folder and run the following command for compiling and executing this module:


java -classpath ".:./jars/*" Main
javac -classpath ".:./jars/*" *.java

The output is the XML file, which is the targeted dictionary.


## Pipeline

Input: XML files extracted from PubMed, which is based on PMIDs “Pipeline/XMLFiles/1186775.XML”.

Dictionary is made by “Making dictionary” module.  “Pipeine/dict/cmDict-NCBI_TAXON.xml”

Gold standard “Pipeline/AnnotationEvaluator/ Workbook2.xlsx”.


Output: Annotated data” Pipeline /output-files-annotator”, labeled pathogens” Pipeline/Label Results”.

Number of pathogens per document and their locations in the text” AnnotationEvaluator /Excel PMID Files”.

Evaluation files “AnnotationEvaluator /Excel Evaluation Files”.


In order to run the pipeline, execute “./run” in the Pipeline.


There are 5 modules in the Pipeline:


###  1- XMLParser

The inputs of this module are XMLFiles and the output format is text, which contains title and abstract. Also each line contains one sentence.
To compile and run this module:

javac -classpath ".:./jars/commons-lang3-3.9.jar" *.java
java -classpath ".:./jars/commons-lang3-3.9.jar" Main



###  2-nlp-pipelines-conceptmapper

This module is ConceptMapper annotator, more information about installing and system requirements could be found in this link:
(https://github.com/UCDenver-ccp/ccp-nlp-pipelines)

To compile and run this module:

mvn clean install

mvn -f nlp-pipelines-conceptmapper/pom.xml exec:java -Dexec.mainClass="edu.ucdenver.ccp.nlp.pipelines.conceptmapper.EntityFinder" -Dexec.args="input-files output-files-annotator NCBI_TAXON ontology-file/ncbitaxon.owl dict False"


### 3- AnnotationEvaluator
This module evaluates the annotator results and provides excel files contain number of pathogens per text and their locations.

To run and compile this module:

javac -classpath ".:./jars/*" *.java
java -classpath ".:./jars/*" Main

### 4- ScientificDiscourseTagging-master

This module provides labels per line for each text input based on: https://arxiv.org/abs/1909.04758

More information about system requirements and installation could be found in this link: https://github.com/jacklxc/ScientificDiscourseTagging

Set the Python path such a way that output file, which called “Predictions”, is generated in the Pipeline folder.

### 5 - Label-assignment

This module assigns labels to the annotated pathogens also provides label sectioning along with true and false positives pathogens.


To compile and run this module:

python label-assignment.py


