
public class LabelConstants {

	public static String CATEGORY_METHOD = "method";
	public static String CATEGORY_RESULT = "result";
	public static String CATEGORY_FACT = "fact";
	public static String CATEGORY_IMPLICATION = "implication";
	public static String CATEGORY_GOAL = "goal";
	public static String CATEGORY_PROBLEM = "problem";
	public static String CATEGORY_HYPOTHESIS = "hypothesis";
	public static String CATEGORY_TITLE = "title";
	public static String CATEGORY_NONE = "none";
	
	
	
}
