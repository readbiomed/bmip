import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

public class Output_PRFileWriter {

	public static String truePositivesFileName = InputParameters.exlEvalFilesFolder+"/"+"True Positives Results.txt";
	public static String falsePositivesFileName = InputParameters.exlEvalFilesFolder+"/"+"False Positives Results.txt";
	public static String falseNegativesFileName = InputParameters.exlEvalFilesFolder+"/"+"False Negatives Results.txt";
	
	
	public static void writePRFiles(ArrayList<FileEntity> fEntities) {
		
		writeFalseNegativeFile(falseNegativesFileName, fEntities);
		writeFalsePositiveFile(falsePositivesFileName, fEntities);
		writeTruePositiveFile(truePositivesFileName, fEntities);
		
	}
	
	private static void writeTruePositiveFile(String fileName , ArrayList<FileEntity> fEntities) {
		
		  FileWriter fileWriter;
		  PrintWriter printWriter = null;
			try {
				fileWriter = new FileWriter(fileName);
				 printWriter   = new PrintWriter(fileWriter);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 
			for (int i = 0; i < fEntities.size(); i++) {
				FileEntity fEntity = fEntities.get(i);
				ArrayList<Entity> truePositiveEntities = fEntity.getTruePositives();
				for (int j = 0; j < truePositiveEntities.size(); j++) {
					Entity entity = truePositiveEntities.get(j);
					 // Remove Similar Items in List
					Set<String> s = new LinkedHashSet<>(entity.getOntology_term());
					ArrayList<String> uniqueOntologyTerms = new ArrayList<>();
					uniqueOntologyTerms.addAll(s);
					////////////////////////////
					for (int k = 0; k < uniqueOntologyTerms.size(); k++) {			
						printWriter.print(fEntity.getId());
						printWriter.print("     ");
						printWriter.print(uniqueOntologyTerms.get(k));
						printWriter.print("     ");
						printWriter.print(entity.getTaxonID());
						printWriter.println();
					}
				}
			}
			printWriter.close();
	}
	
	
	private static void writeFalsePositiveFile(String fileName , ArrayList<FileEntity> fEntities) {
		
		  FileWriter fileWriter;
		  PrintWriter printWriter = null;
			try {
				fileWriter = new FileWriter(fileName);
				 printWriter   = new PrintWriter(fileWriter);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 
			for (int i = 0; i < fEntities.size(); i++) {
				FileEntity fEntity = fEntities.get(i);
				ArrayList<Entity> falsePositiveEntities = fEntity.getFalsePositives();
				for (int j = 0; j < falsePositiveEntities.size(); j++) {
					Entity entity = falsePositiveEntities.get(j);
					 // Remove Similar Items in List
					Set<String> s = new LinkedHashSet<>(entity.getOntology_term());
					ArrayList<String> uniqueOntologyTerms = new ArrayList<>();
					uniqueOntologyTerms.addAll(s);
					////////////////////////////
					for (int k = 0; k < uniqueOntologyTerms.size(); k++) {			
						printWriter.print(fEntity.getId());
						printWriter.print("     ");
						printWriter.print(uniqueOntologyTerms.get(k));
						printWriter.print("     ");
						printWriter.print(entity.getTaxonID());
						printWriter.println();
					}
				}
			}
			printWriter.close();
	}
	
	
	
	private static void writeFalseNegativeFile(String fileName , ArrayList<FileEntity> fEntities) {
		
		  FileWriter fileWriter;
		  PrintWriter printWriter = null;
			try {
				fileWriter = new FileWriter(fileName);
				 printWriter   = new PrintWriter(fileWriter);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 
			for (int i = 0; i < fEntities.size(); i++) {
				FileEntity fEntity = fEntities.get(i);
				ArrayList<Entity> falseNegativeEntities = fEntity.getFalseNegatives();
				for (int j = 0; j < falseNegativeEntities.size(); j++) {
					Entity entity = falseNegativeEntities.get(j);
					 // Remove Similar Items in List
					Set<String> s = new LinkedHashSet<>(entity.getOntology_term());
					ArrayList<String> uniqueOntologyTerms = new ArrayList<>();
					uniqueOntologyTerms.addAll(s);
					////////////////////////////
					for (int k = 0; k < uniqueOntologyTerms.size(); k++) {			
						printWriter.print(fEntity.getId());
						printWriter.print("     ");
						printWriter.print(uniqueOntologyTerms.get(k));
						printWriter.print("     ");
						printWriter.print(entity.getTaxonID());
						printWriter.println();
					}
				}
			}
			printWriter.close();
	}
	
}
