import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

public class ReadInputFile {

	public static HashMap<String, String> read(File file , ArrayList<String> searchItems) {
		
		HashMap<String, String> data = new HashMap<>();
		Scanner sc;
		try {
			sc = new Scanner(file);
			while (sc.hasNextLine()) { 
				String line = sc.nextLine();
				for (int i = 0; i < searchItems.size(); i++) {
					String startTag = "<"+searchItems.get(i)+">";
					String endTag = "</"+searchItems.get(i)+">";
					if (line.contains(startTag)) {
						data.put(searchItems.get(i), StringUtils.substringBetween(line,startTag, endTag));
						break;
					}
				}
		  } 
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return data;
	}
	
}
