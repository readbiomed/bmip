
import java.io.File;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

public class Main {

	public static void main(String[] args) {

		/// Build Individual and Merged Excel Files from txt inputs
		ArrayList<FileEntity> fEntities = BuildFileEntities.build();

		//// Evaluation and compare with Reference File
		for (int i = 0; i < fEntities.size(); i++) {
			FileEntity fEntity = fEntities.get(i);

			ArrayList<Entity> reference_entities = Reference_ExcelFileReader.getReferenceItems(fEntity.getId());
			ArrayList<String> items_reference = new ArrayList<>();
			for (int j = 0; j < reference_entities.size(); j++) {
				if (InputParameters.Evaluation_Mode == EvaluationMode.BY_TAXONID)
					items_reference.add(reference_entities.get(j).getTaxonID());
				else if (InputParameters.Evaluation_Mode == EvaluationMode.BY_ONTOLOGY)
					items_reference.add(reference_entities.get(j).getOntology_term().get(0));
			}

			ArrayList<Entity> result_entities = fEntity.getEntities();
			ArrayList<String> items_results = new ArrayList<>();
			for (int j = 0; j < result_entities.size(); j++) {
				if (InputParameters.Evaluation_Mode == EvaluationMode.BY_TAXONID)
					items_results.add(result_entities.get(j).getTaxonID());
				else if (InputParameters.Evaluation_Mode == EvaluationMode.BY_ONTOLOGY)
					items_results.add(result_entities.get(j).getOntology_term().get(0));
			}

			// We have Two Arrays including refernce and result (txt file) values to be
			// compared
			ArrayList<Entity> falsePositives = new ArrayList<>();
			ArrayList<Entity> falseNegatives = new ArrayList<>();
			ArrayList<Entity> truePositives = new ArrayList<>();

			for (int j = 0; j < items_reference.size(); j++) {
				String onr = items_reference.get(j);
				if (items_results.contains(onr))
					truePositives.add(reference_entities.get(j));
				else
					falseNegatives.add(reference_entities.get(j));
			}

			for (int j = 0; j < items_results.size(); j++) {
				String onr = items_results.get(j);
				if (!items_reference.contains(onr))
					falsePositives.add(result_entities.get(j));
			}

			fEntity.setTruePositives(truePositives);
			fEntity.setFalseNegatives(falseNegatives);
			fEntity.setFalsePositives(falsePositives);
		}

		/////////////////// Delete previous files if exist//////////////////////////
		    DeletePreviousFiles.delete(InputParameters.exlPMIDFilesFolder);
			DeletePreviousFiles.delete(InputParameters.exlEvalFilesFolder);
		/////////////////////////////////////////////////////////

		//////////////////// WRITE NEW EXCEL FILES///////////////////////////////////
		for (int i = 0; i < fEntities.size(); i++) {
			FileEntity fEntity = fEntities.get(i);
			Output_ExcelFileWriter.writeFileEntityExcelFile(fEntity);
		}
		if (InputParameters.Evaluation_Mode == EvaluationMode.BY_ONTOLOGY)
			Output_ExcelFileWriter.writeMergedFiles(fEntities);

		//////////////////////// Write Number Report File/////////////////////
		Output_ExcelFileWriter.writeNumberReportFile(fEntities);
		///////////////////////////////////////////////////////////////////////

		/////////////// Write Evaluation File/////////////////////////
		Output_ExcelFileWriter.writeMainEvaluationFile(fEntities);
		Output_PRFileWriter.writePRFiles(fEntities);
		Output_ExcelFileWriter.writeLabelEvaluationFile(fEntities);
/////////////////////////////////////////////////////////////////////////////////////
		PrintTotalPR.print(fEntities);
/////////////////////////////////////////////////////////////////////////////////////
		
	}

}

