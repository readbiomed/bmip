import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

public class FindAllLevelSubclasses {

	public static int traverse(List<String> searchItemsList_TaxonIDs_CurrentLevel , ArrayList<OwlClass> owlClassesResuts) {
		
		List<String> searchItemsList_TaxonIDs_NextLevel = new ArrayList<>();
		
		try (Scanner sc = new Scanner(new File(InputParameters.OWLFileName), "UTF-8")) {

			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				/////// If a new owl class is found ///////////
				if (line.contains("<owl:Class")) {
					String owlClassString = line + "\n";
					while (sc.hasNextLine()) {
						String classLine = sc.nextLine();
						owlClassString += classLine + "\n";
						if (classLine.endsWith("</owl:Class>")) {
							break;
						}
					} // Raw String of owl class is built

					
					// Finding Potential Axioms After Class
					ArrayList<String> rawAxiomStringsforCurrentClass = new ArrayList<>();
					while (sc.hasNextLine()) {
						line = sc.nextLine();
						if (line.contains("<owl:Axiom>")) {
							String axiomString = line + "\n";
							while (sc.hasNextLine()) {
								String axiomLine = sc.nextLine();
								axiomString += axiomLine + "\n";
								if (axiomLine.endsWith("</owl:Axiom>")) {
									rawAxiomStringsforCurrentClass.add(axiomString);
									break;
								}
							}
						} else
							break;
					} // End Finding Potential Axioms After Class
				
					
					// Checking whether the owl class is subclass of our search item or not

				
					String  stringToSearch_subclass = StringUtils.substringBetween(owlClassString, "<rdfs:subClassOf rdf:resource=\"", "\"");
					
					// check cloned_searchItemsList null or not
					if (searchItemsList_TaxonIDs_CurrentLevel.contains(stringToSearch_subclass)) {
						OwlClass owlclass = new OwlClass();
						owlclass.setRawOwlClassString(owlClassString);
						owlclass.setId(StringUtils.substringBetween(owlClassString, "rdf:about=\"", "\""));
						owlclass.setCanonical(StringUtils.substringBetween(owlClassString,
								"<rdfs:label rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">",
								"</rdfs:label>"));
				
						// Add Axiom Information to Owl Class Object
						for (int i = 0; i < rawAxiomStringsforCurrentClass.size(); i++) {
							String axiomString = rawAxiomStringsforCurrentClass.get(i);
							owlclass.getRawAxiomStrings().add(axiomString);
							owlclass.getVariants().add(StringUtils.substringBetween(axiomString,
									"<owl:annotatedTarget rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">",
									"</owl:annotatedTarget>"));
						}
						////////////// Axiom by Stop Words//////////////
							Utils.addStopWordAxiom(owlclass);
						/////////////////////////////////////////////////////////////////

				if (!Utils.hasSameOwlClass(owlclass, owlClassesResuts)) {
						searchItemsList_TaxonIDs_NextLevel.add(owlclass.getId());
						owlClassesResuts.add(owlclass);
				}
					}
					// Owl class is not a subclass
				}
				
			}

			if (sc.ioException() != null) {
				try {
					throw sc.ioException();
				} catch (IOException e) {

					e.printStackTrace();
				}
			}
		} catch (FileNotFoundException e1) {

			e1.printStackTrace();
		}
		
		if (searchItemsList_TaxonIDs_NextLevel.isEmpty())
			return 0;
		else
			traverse(searchItemsList_TaxonIDs_NextLevel, owlClassesResuts);
		
		return searchItemsList_TaxonIDs_NextLevel.size();
	}
	
	
}
