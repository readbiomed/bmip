import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

public class LabelFilesReader {

	public static int getCount(String PMID, String TaxonID, String category) {

		int count = 0;

		File folderLabelFilePaths = new File(InputParameters.labelFilesFolder);
		File file = new File(folderLabelFilePaths + "/" + "label-" + PMID + ".txt");

		
		if (file.exists()) {
			try (Scanner sc = new Scanner(file, "UTF-8")) {

				while (sc.hasNextLine()) {

					String line = sc.nextLine();

					///////// Parse Line T
					Scanner scTemp = new Scanner(line);
					String fileTaxonID = scTemp.next();
					String fileCategory = scTemp.next();

//					System.out.println("TaxonID:"+TaxonID);
//					System.out.println("fileTaxonID:"+fileTaxonID);
					
					if (TaxonID.equals(fileTaxonID) && category.equals(fileCategory)) {
						count++;
					}
				}
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}

		return count;

	}
}