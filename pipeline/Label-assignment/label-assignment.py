#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 14 21:19:28 2020

@author: Leyla
"""
import numpy
import pandas as pd
import re
from collections import defaultdict
#from itertools import izip
#import collections.OrderedDict
import itertools
import collections, functools, operator 
ll=[]
FN=[]
pattern=[]
finallabels=[]
Truepositive=[]
Falsepositive=[]
PMIDs = pd.read_excel (r'../AnnotationEvaluator/Workbook2.xlsx')
sizeP=PMIDs.shape
#print(type(sizeP[0]))
pmids=PMIDs.loc[:, "PMID"]
#print(pmids)
u=0
rr=[]
for e in pmids:
    u=u+1
    noon=(e,u-1)
    rr.append(noon)
#print(rr) 
    
for pp in rr:
                print(pp[0])
                s=[]
                with open('../input-files/'+str(pp[0])+'.txt') as myFile:
                    for line in myFile.readlines():
                        s.append(line) 
                        #print(line)
                        
                    print(len(s))
                k=0 
                kk=0
                kkk=0
                final = numpy.empty((0,3), int)
                count=0
                for i in range(0,len(s)):
                    
                    l=0
                    for ii, c in enumerate(s[i]):
                        l=l+1
                        ii=k+ii+1
                        if l==1:
                            KSTART=ii
                        
                        #print(ii, c) 
                        
                    k=ii
                    kEnd=k
                    kt=[i,KSTART-1,kEnd-1]
                    final=numpy.append(final,numpy.array([kt]),axis=0)
                    #print(final)
                    if s[i] == "\n":
                            count=count+1
                            if count == 1:
                                    print(kt)
                                    newlabel=["title",KSTART-1,kEnd-1]
                                    t=1
                                    kkk=k
                                    while s[i+t] !="\n":
                                        t=t+1
                                        lll=0
                                        for iiii, ccc in enumerate(s[i+t-1]):
                                            lll=lll+1
                                            iiii=kkk+iiii+1
                                            if lll==1:
                                                KSTART2=iiii
                                     
                                            
                                            #print(ii, c) 
                                            
                                        kkk=iiii
                                        kEnd2=kkk
                                        newlabel=["title",KSTART-1,kEnd2-1]
                                    print(newlabel)
                                        
                                       
                    print(kt)
                print(final)
                file1 = open('../predictions/'+str(pp[0])+'att=True_cont=LSTM_clause_lstm=False_bi=True_crf=True.out')
                strings1 = [line.strip() for line in file1]   
                #print(len(strings1))
                final1 = numpy.empty((0,4), int)
                for iu in range(1,len(final)):
                                   
                    if  newlabel[1] <= int(final[iu][2]) < newlabel[2]:
                        t=numpy.append("title",[final[iu]])
                        #print("this is true")
                    else :
                        t=numpy.append(strings1[iu-1],[final[iu]])
                    #print(t)
                    final1=numpy.append(final1,numpy.array([t]),axis=0)
                print(final1) 
                
                df = pd.read_excel (r'../AnnotationEvaluator/Excel PMID Files/'+str(pp[0])+'.xlsx')
                #print (df)
                size=df.shape
                
                KE=[]
                D={}
                for tt in range(0,size[0]):
                    v=df.loc[tt, "taxon ID"]
                    table=df.loc[tt, "location(s)"]
                    K=df.loc[tt, "taxon ID"]
                    KE.append(K)
                    #print(table)
                    result = [x1.strip() for x1 in table.split(',')]
                    #print(len(result))
                    a = numpy.empty((0,2), int)
                    #a=numpy.array([])
                    for y in result:
                        #print(y)
                        d1=[x2.strip() for x2 in y.split('--')]
                        #print(d1)
                        characters_to_remove ="]["
                        pattern = "[" + characters_to_remove + "]"
                        position=[]
                        #print(d1)
                        for z in d1:
                          new_string = re.sub(pattern, "", z)
                          new_string=int(new_string)
                          
                          position.append(new_string)
                        
                        a=numpy.append(a,numpy.array([position]),axis=0)
                        #print(a)
                    #print(K)
                    D[K]=a
                #print(D) 
                Llabel= numpy.empty((0,2), int)
                for key, value in D.items():
                    #print(key)
                    #print(value)
                    for i in value:
                        #print(i[0])
                        for nn in final1:
                            p1=int(nn[3])
                            p2=int(nn[2])
                            if p1>i[0]>p2:
                                #print(nn[0])
                                #print(key)
                                lastlabel=(key,nn[0])
                                #print(lastlabel)
                                Llabel=numpy.append(Llabel,numpy.array([lastlabel]),axis=0)
                print(Llabel) 
                taxon1=PMIDs.loc[pp[1], "Taxon-id 1"]
                #print("this is taxon"+str(taxon1))
                taxon2=PMIDs.loc[pp[1], "Taxon-id 2"]
                taxon3=PMIDs.loc[pp[1], "Taxon-id 3"]
                taxon4=PMIDs.loc[pp[1],"Taxon-id 4"]
                taxon5=PMIDs.loc[pp[1], "Taxon-id 5"]
                dicts=[]
                for DD in Llabel:
                    
                    d1={DD[0] : DD[1]}
                    dicts.append(d1)
                print("this is dicts"+str(dicts))
                everysingle=[i for n, i in enumerate(dicts) if i not in dicts[n + 1:]]
            
                
                super_dict = defaultdict(set)  # uses set to avoid duplicates

                for d in dicts:
                    for k, v in d.items():  # use d.iteritems() in python 2
                        super_dict[k].add(v)    
                    
                    
                     
                print("this is superdict"+str(super_dict))
                                        
                for k,v in super_dict.items():
                    newdict=list(v)
                    newdict=sorted(newdict, key=str.lower)
                    
                    my_lst_str='-'.join(newdict)
                    finallabels.append(my_lst_str)
                    
                
                
                f=open('../Label Results/label-'+str(pp[0])+'.txt','w')
                for ele in Llabel:
                    f.write(ele[0]+'\t'+ele[1]+'\n')
                f.close()
                
                fortestFP=[]
                fortestTP=[]
                for key,value in super_dict.items():
                    newvalue1=list(value)
                    if key==str(taxon1):
                        
                       nvaluesort1=sorted(newvalue1, key=str.lower)
                       #my_lst_str = ''.join(map(str, newdict))
                       sortedvalue1='-'.join(nvaluesort1)
                       Truepositive.append(sortedvalue1)
                       fortestTP.append(sortedvalue1)
                    elif key==str(taxon2):
                       newvalue1=list(value) 
                       nvaluesort1=sorted(newvalue1, key=str.lower)
                       #my_lst_str = ''.join(map(str, newdict))
                       sortedvalue1='-'.join(nvaluesort1)
                       Truepositive.append(sortedvalue1)
                       fortestTP.append(sortedvalue1)
                    elif key==str(taxon3):
                       newvalue1=list(value) 
                       nvaluesort1=sorted(newvalue1, key=str.lower)
                       #my_lst_str = ''.join(map(str, newdict))
                       sortedvalue1='-'.join(nvaluesort1)
                       Truepositive.append(sortedvalue1)
                       fortestTP.append(sortedvalue1)
                    elif key==str(taxon4):
                       newvalue1=list(value) 
                       nvaluesort1=sorted(newvalue1, key=str.lower)
                       #my_lst_str = ''.join(map(str, newdict))
                       sortedvalue1='-'.join(nvaluesort1)
                       Truepositive.append(sortedvalue1)  
                       fortestTP.append(sortedvalue1)
                    elif key==str(taxon5):
                       newvalue1=list(value) 
                       nvaluesort1=sorted(newvalue1, key=str.lower)
                       #my_lst_str = ''.join(map(str, newdict))
                       sortedvalue1='-'.join(nvaluesort1)
                       Truepositive.append(sortedvalue1)
                       fortestTP.append(sortedvalue1)
                    else:
                       newvalue1=list(value) 
                       nvaluesort1=sorted(newvalue1, key=str.lower)
                       #my_lst_str = ''.join(map(str, newdict))
                       sortedvalue1='-'.join(nvaluesort1)
                       Falsepositive.append(sortedvalue1)
                       fortestFP.append(sortedvalue1)
                    

                
                llD=[]
                for hh in Llabel:
                   if str(hh[0])==str(taxon1):
                       (hh[0],hh[1])
                       ll.append(hh[1])
                       llD.append
                   elif str(hh[0])==str(taxon2):
                       ll.append(hh[1])
                   elif str(hh[0])==str(taxon3):
                       ll.append(hh[1]) 
                   elif str(hh[0])==str(taxon4):
                       ll.append(hh[1])      
                   elif str(hh[0])==str(taxon5):
                       ll.append(hh[1]) 
                   else:
                       FN.append(hh[1])
                     
#bb=ll.tolist()   
#print(type(ll))
d = {x:ll.count(x) for x in ll}
d11 = {x:FN.count(x) for x in FN}
pat= {xj:finallabels.count(xj) for xj in finallabels}
newpat={k: v for k, v in sorted(pat.items(), key=lambda item: item[1])}
#print("this is tP"+str(Truepositive))
ForTP={y:Truepositive.count(y) for y in Truepositive}
ForFP={y1:Falsepositive.count(y1) for y1 in Falsepositive}
newpat1={k1: v1 for k1, v1 in sorted(ForTP.items(), key=lambda item: item[1])}
#print(d)
#print(d11)
#print(pat)
#print(newpat)
#print(newpat1)
#print(ForFP)
dicts1 = [newpat1, ForFP]
Bigdict = defaultdict(list)
counter=0
for myd in dicts1:
    counter=1+counter
    for kk, vv in myd.items():
        if counter==1:
           Bigdict[kk].append(str(vv)+"TP")  
        else:
           Bigdict[kk].append(str(vv)+"FP")
#print("this is TF combination"+str(Bigdict))  
Allinone=[] 
for key1,value1 in Bigdict.items():   
    lensection=len(key1.split("-"))
    single={lensection:[key1,value1]}
    Allinone.append(single)
    
#print(Allinone)
All = defaultdict(list)
for s in Allinone:
    
  for kkk,vvv in s.items():
    All[kkk].append(vvv)   
Allsorted={k11: v11 for k11, v11 in sorted(All.items(), key=lambda item: item[0])}         
dicts11=[]               
for p0,p1 in newpat1.items():

      d11=[x3.strip() for x3 in p0.split('-')]
      
      for i in range(1,len(d11)+1):
        itlist=list(itertools.combinations(d11,i))
        for nn in range(0,len(itlist)):
            ff=list(itlist[nn])
            ffsorted=sorted(ff, key=str.lower)
            jointffsorted='-'.join(ffsorted)
            dicts11.append({jointffsorted:p1} )

result1 = dict(functools.reduce(operator.add,map(collections.Counter, dicts11)))
#print(result1)

dicts12=[]               
for p0,p1 in ForFP.items():

      d12=[x4.strip() for x4 in p0.split('-')]
      
      for i in range(1,len(d12)+1):
        itlist1=list(itertools.combinations(d12,i))
        for nn in range(0,len(itlist1)):
            ff1=list(itlist1[nn])
            ff1sorted=sorted(ff1, key=str.lower)
            jointff1sorted='-'.join(ff1sorted)
            dicts12.append({jointff1sorted:p1} )


result2 = dict(functools.reduce(operator.add,map(collections.Counter, dicts12)))

dicts13 = [result1, result2]
Bigdict13 = defaultdict(list)
counter=0
for myd in dicts13:
    counter=1+counter
    for kk, vv in myd.items():
        if counter==1:
           Bigdict13[kk].append(str(vv)+"TP")  
        else:
           Bigdict13[kk].append(str(vv)+"FP") 
Allinone1=[] 
for key1,value1 in Bigdict13.items():   
    lensection1=len(key1.split("-"))
    single1={lensection1:[key1,value1]}
    Allinone1.append(single1)
    if len(value1)==2:
       #print(value1[1]) 
       if 2*int(value1[0][:-2])>=int(value1[1][:-2]):
           print("this is single1"+str(single1))
        
    

All1 = defaultdict(list)
for s in Allinone1:
    
  for kkk,vvv in s.items():
    All1[kkk].append(vvv) 
       
Allsorted1={k11: v11 for k11, v11 in sorted(All1.items(), key=lambda item: item[0])}

print(All1)    


  
