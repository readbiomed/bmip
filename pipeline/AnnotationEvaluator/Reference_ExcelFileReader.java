import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Reference_ExcelFileReader {

	public static ArrayList<Entity> getReferenceItems(String fileEntityID) {
		ArrayList<Entity> entities = new ArrayList<>();
		try {
			FileInputStream file = new FileInputStream(new File(InputParameters.exlReferenceFileInput));

			XSSFWorkbook workbook = new XSSFWorkbook(file);
			XSSFSheet sheet = workbook.getSheetAt(0);

			Iterator<Row> rowIterator = sheet.iterator();
			Row row = rowIterator.next(); // First Row contain Titles
			while (rowIterator.hasNext()) {
				row = rowIterator.next();
				Cell cell = row.getCell(0);

				if (fileEntityID.equals(String.valueOf(Double.valueOf(cell.getNumericCellValue()).longValue()))) {
					Iterator<Cell> cellIterator = row.cellIterator();
					cellIterator.next(); // First cell is PMID Value
					while (cellIterator.hasNext()) {
						Cell cell_pathogen = cellIterator.next(); // Read Pathogen name
						//System.out.println("*****"+fileEntityID+"******");
						Cell cell_taxonID = cellIterator.next(); // Read TaxonID field

//							if (InputParameters.Evaluation_Mode == EvaluationMode.BY_TAXONID)
//								cell = cell_taxonID;
//							else if (InputParameters.Evaluation_Mode == EvaluationMode.BY_ONTOLOGY)
//								cell = cell_pathogen;
						Entity entity = new Entity();
						
							switch (cell_pathogen.getCellType()) {
							case NUMERIC: 
								entity.getOntology_term().add((String.valueOf(Double.valueOf(cell_pathogen.getNumericCellValue()).longValue())));
								break;
							case STRING:
								entity.getOntology_term().add(cell_pathogen.getStringCellValue());
								break;
							}
							
							switch (cell_taxonID.getCellType()) {
							case NUMERIC:
								entity.setTaxonID((String.valueOf(Double.valueOf(cell_taxonID.getNumericCellValue()).longValue())));
								break;
							case STRING:
								entity.setTaxonID(cell_taxonID.getStringCellValue());
								break;
							}
					entities.add(entity);	
					}
					break; // ?
				}
			}
			
			file.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return entities;
	}

	public static ArrayList<String> getPMIDValues() {
		ArrayList<String> pmidValues = new ArrayList<>();
		try {
			FileInputStream file = new FileInputStream(new File(InputParameters.exlReferenceFileInput));
			XSSFWorkbook workbook = new XSSFWorkbook(file);
			XSSFSheet sheet = workbook.getSheetAt(0);

			Iterator<Row> rowIterator = sheet.iterator();
			Row row = rowIterator.next(); // First Row contain Titles
			while (rowIterator.hasNext()) {
				row = rowIterator.next();
				Cell cell = row.getCell(0);
				pmidValues.add(String.valueOf(Double.valueOf(cell.getNumericCellValue()).longValue()));
			}

			// for (int i = 0; i < pmidValues.size(); i++) {
			// System.out.println(pmidValues.get(i));
			// }

			file.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pmidValues;
	}

}