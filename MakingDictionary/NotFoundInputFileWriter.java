import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class NotFoundInputFileWriter {

	
	private PrintWriter printWriter;
	
	public NotFoundInputFileWriter(String fileName) {
		  FileWriter fileWriter;
		try {
			fileWriter = new FileWriter(fileName);
			 printWriter   = new PrintWriter(fileWriter);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		   
	}
	
	
	
	public void write(List<String> searchItemsList) {
		 
		 for (int i = 0; i < searchItemsList.size(); i++) {
			 printWriter.println(searchItemsList.get(i));
		 }
		
	}
	
	public void finishWriting() {
		 
		printWriter.close();
		
	}
	
}