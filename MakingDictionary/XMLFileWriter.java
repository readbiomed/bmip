
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class XMLFileWriter {

	static String staticTaxonID_Pref = "http://purl.obolibrary.org/obo/NCBITaxon_D";
	static int staticTaxonID_num = 0;									
	static String staticCanonical = "Unknown Type";
	
	private PrintWriter printWriter;
	
	public XMLFileWriter(String fileName) {
		  FileWriter fileWriter;
		try {
			fileWriter = new FileWriter(fileName);
			 printWriter   = new PrintWriter(fileWriter);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		   
	}
	
	
	public void beginWriting() {
		 printWriter.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		 printWriter.println("<synonym>");
	}
	
	public void writeFoundToken(String ID, String canonical, ArrayList<String> variants) {
		 printWriter.print("<token ");
		 
		 
		 printWriter.print("id= " + "\"" + ID + "\"" + " ");
		 printWriter.print("canonical= " + "\"" + canonical + "\"");
		 printWriter.println(">");
		 
		 for (int i = 0; i < variants.size(); i++) {
			 printWriter.print("	<variant ");
			 printWriter.print("base= " + "\"" + variants.get(i) + "\"");
			 printWriter.println("/>");
		 }
		 // Add Canonical as a Variant if no Variant exist
		 if (!variants.contains(canonical)) {
			 printWriter.print("	<variant ");
			 printWriter.print("base= " + "\"" + canonical + "\"");
			 printWriter.println("/>");
		 }
		////////////////////////////////////////////////////////
		 
		// Improve Accuracy of Dictionary
		 String canonicalPlus = canonical.replace("_", " ");
		 if (canonical.contains("_") && !variants.contains(canonicalPlus)) {
			 printWriter.print("	<variant ");
			 printWriter.print("base= " + "\"" + canonicalPlus + "\"");
			 printWriter.println("/>");
		 }
		 ////////////////////////////////////////////////////////
		 
		 printWriter.println("</token>");
	}
	
	
	public void appendNotFoundToken(String canonical) {
		 staticTaxonID_num ++;
			
			 printWriter.print("<token ");
			 
			 
			 printWriter.print("id= " + "\"" + staticTaxonID_Pref + staticTaxonID_num + "\"" + " ");
			 printWriter.print("canonical= " + "\"" + canonical + "\"");
			 printWriter.println(">");
			 
			 // Add Canonical as a Variant if no Variant exist
				 printWriter.print("	<variant ");
				 printWriter.print("base= " + "\"" + canonical + "\"");
				 printWriter.println("/>");
			////////////////////////////////////////////////////////
			
		
			 
			 printWriter.println("</token>");
		}
	
	
//	public void writeUnFoundToken(String item) {
//		 staticTaxonID_num ++;
//		
//		 printWriter.print("<token ");
//		 
//		 if (InputParameters.searchMode==SearchMode.BY_LABEL) {
//		 printWriter.print("id= " + "\"" + staticTaxonID_Pref + staticTaxonID_num + "\"" + " ");
//		 printWriter.print("canonical= " + "\"" + item + "\"");
//		 printWriter.println(">");
//		 
//		 // Add Canonical as a Variant if no Variant exist
//			 printWriter.print("	<variant ");
//			 printWriter.print("base= " + "\"" + item + "\"");
//			 printWriter.println("/>");
//		////////////////////////////////////////////////////////
//		 }
//		 else if (InputParameters.searchMode==SearchMode.BY_TAXONID) {
//			 printWriter.print("id= " + "\"" + item + "\"" + " ");
//			 printWriter.print("canonical= " + "\"" + staticCanonical + "\"");
//			 printWriter.println(">");
//			 
//			 // Add Canonical as a Variant if no Variant exist
//				 printWriter.print("	<variant ");
//				 printWriter.print("base= " + "\"" + staticCanonical + "\"");
//				 printWriter.println("/>");
//			////////////////////////////////////////////////////////
//		 }
//		 
//		 printWriter.println("</token>");
//	}
	
	public void finishWriting() {
		 printWriter.print("</synonym>");
		 printWriter.close();
	}
	
	
}
