import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

public class StopWordsFileReader {
	public static ArrayList<String> read (String fileName) {

		ArrayList<String> stopWordsItems = new ArrayList<>();
		

		try (Scanner sc = new Scanner(new File(fileName), "UTF-8")) {
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				stopWordsItems.add(line);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	        
	        return stopWordsItems;
	        
	    }

}
