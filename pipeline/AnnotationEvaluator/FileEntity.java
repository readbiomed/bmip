
import java.util.ArrayList;

public class FileEntity {

	String id;
	ArrayList<Entity> entities;

	private ArrayList<Entity> falsePositives;
	private ArrayList<Entity> falseNegatives;
	private ArrayList<Entity> truePositives;

	public FileEntity() {
		entities = new ArrayList<>();
		
		falseNegatives= new ArrayList<>();
		falsePositives= new ArrayList<>();
		truePositives= new ArrayList<>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ArrayList<Entity> getEntities() {
		return entities;
	}

	public void setEntities(ArrayList<Entity> entities) {
		this.entities = entities;
	}

	public ArrayList<Entity> getFalsePositives() {
		return falsePositives;
	}

	public void setFalsePositives(ArrayList<Entity> falsePositives) {
		this.falsePositives = falsePositives;
	}

	public ArrayList<Entity> getFalseNegatives() {
		return falseNegatives;
	}

	public void setFalseNegatives(ArrayList<Entity> falseNegatives) {
		this.falseNegatives = falseNegatives;
	}

	public ArrayList<Entity> getTruePositives() {
		return truePositives;
	}

	public void setTruePositives(ArrayList<Entity> truePositives) {
		this.truePositives = truePositives;
	}



}
