import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Color;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Output_ExcelFileWriter {
	
	
	
	public static void writeFileEntityExcelFile(FileEntity fEntity) 
    {
        //Blank workbook
        XSSFWorkbook workbook = new XSSFWorkbook(); 
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        
        //Create a blank sheet
        XSSFSheet sheet = workbook.createSheet();
          
        //This data needs to be written (Object[])
        Map<String, ArrayList<Object>> data = new TreeMap<String, ArrayList<Object>>();
        
        
        ArrayList<Object> titles = new ArrayList<>();
        titles.add("ontology_name");
        titles.add("taxon ID");
        titles.add("number of repetitions");
        titles.add("location(s)");
        data.put("1", titles);
    
		
			for (int i = 0; i < fEntity.getEntities().size(); i++) {
				Entity en = fEntity.getEntities().get(i);
				ArrayList<Object> enArr = new ArrayList<>();
				
				String ontology_term = "";
				for (int j = 0; j < en.getOntology_term().size(); j++) {
					ontology_term += ("[" + en.getOntology_term().get(j) + "]");
					if (j!=en.getOntology_term().size()-1)
						ontology_term +=",";
				}
				enArr.add(ontology_term);
				enArr.add(en.getTaxonID());
				enArr.add(en.getLocations().size());
				String locations = "";
				for (int j = 0; j < en.getLocations().size(); j++) {
					locations += en.getLocations().get(j);
					if (j!=en.getLocations().size()-1)
						locations +=",";
				}
				enArr.add(locations);
				data.put(String.valueOf(i+2), enArr);
			}
		
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Iterate over data and write to sheet
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset)
        {
            Row row = sheet.createRow(rownum++);
            ArrayList<Object> objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr)
            {
               Cell cell = row.createCell(cellnum++);
              cell.setCellStyle(cellStyle);
               if(obj instanceof String)
                    cell.setCellValue((String)obj);
                else if(obj instanceof Integer)
                    cell.setCellValue((Integer)obj);
            }
        }
        try
        {
            //Write the workbook in file system
        	File file = new File(InputParameters.exlPMIDFilesFolder);
        	file.mkdirs();
            FileOutputStream out = new FileOutputStream(new File(InputParameters.exlPMIDFilesFolder+"/"+fEntity.getId()+".xlsx"));
            workbook.write(out);
            out.close();
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
      
        
    }
	
	
	 public static void writeMergedFiles(ArrayList<FileEntity> fEntities) {
     	
		   //Blank workbook
	        XSSFWorkbook workbook = new XSSFWorkbook(); 
	        CellStyle cellStyle = workbook.createCellStyle();
	        cellStyle.setAlignment(HorizontalAlignment.CENTER);
	        
	        //Create a blank sheet
	        XSSFSheet sheet = workbook.createSheet();
	          
	        //This data needs to be written (Object[])
	        Map<String, ArrayList<Object>> data = new TreeMap<String, ArrayList<Object>>();
	        int maxNumberofPathogen = 0;
	        
				for (int i = 0; i < fEntities.size(); i++) {
					FileEntity fEntity = fEntities.get(i);
					ArrayList<Object> enArr = new ArrayList<>();
					enArr.add(fEntity.getId());

					for (int j = 0; j < fEntity.getEntities().size(); j++) {
						Entity en = fEntity.getEntities().get(j);
						enArr.add(en.getOntology_term().get(0));
						if (maxNumberofPathogen < fEntity.getEntities().size())
							maxNumberofPathogen = fEntity.getEntities().size();
					}
					data.put(String.valueOf(i+2), enArr);
				}
				
				//// Pathogen Titles
				ArrayList<Object> titles = new ArrayList<>();
				titles.add("PMID");
				for (int i = 0; i < maxNumberofPathogen; i++) {
					titles.add("Pathogen" + (i+1));
				}
				data.put("1", titles);
				/////////////////////////////////////////////
				
	        ////////////////////////////////////////////////////////////////////////////////////////////////////////
	        //Iterate over data and write to sheet
	        Set<String> keyset = data.keySet();
	        int rownum = 0;
	        for (String key : keyset)
	        {
	            Row row = sheet.createRow(rownum++);
	            ArrayList<Object> objArr = data.get(key);
	            int cellnum = 0;
	            for (Object obj : objArr)
	            {
	               Cell cell = row.createCell(cellnum++);
	              cell.setCellStyle(cellStyle);
	               if(obj instanceof String)
	                    cell.setCellValue((String)obj);
	                else if(obj instanceof Integer)
	                    cell.setCellValue((Integer)obj);
	            }
	        }
	        try
	        {
	            //Write the workbook in file system
	        	File file = new File(InputParameters.exlEvalFilesFolder);
	        	file.mkdirs();
	            FileOutputStream out = new FileOutputStream(new File(InputParameters.exlEvalFilesFolder+"/"+InputParameters.exlMergedFilesOutputName));
	            workbook.write(out);
	            out.close();
	        } 
	        catch (Exception e) 
	        {
	            e.printStackTrace();
	        }
	      
     }
     
	 
	 
	 
	 public static void writeMainEvaluationFile(ArrayList<FileEntity> fEntities) {
	     	
		   //Blank workbook
	        XSSFWorkbook workbook = new XSSFWorkbook(); 
	        CellStyle cellStyle = workbook.createCellStyle();
	        cellStyle.setAlignment(HorizontalAlignment.CENTER);
	        
	        //Create a blank sheet
	        XSSFSheet sheet = workbook.createSheet();
	          
	        //This data needs to be written (Object[])
	        Map<String, ArrayList<Object>> data = new TreeMap<String, ArrayList<Object>>();
	       
	        ArrayList<Object> titles = new ArrayList<>();
			titles.add("PMID");
			titles.add("True Positive");
			titles.add("False Negative");
			titles.add("False Positive");
			titles.add("Precision");
			titles.add("Recall");
			
			data.put("1", titles);
	        
				for (int i = 0; i < fEntities.size(); i++) {
					FileEntity fEntity = fEntities.get(i);
					ArrayList<Object> arr = new ArrayList<>();
					arr.add(fEntity.getId());
					arr.add(fEntity.getTruePositives().size());
					arr.add(fEntity.getFalseNegatives().size());
					arr.add(fEntity.getFalsePositives().size());
					
					

					double precision =0;
					double recall =0;
					if (fEntity.getTruePositives().size() + fEntity.getFalsePositives().size() != 0)
					 precision = (double)(fEntity.getTruePositives().size()) / (fEntity.getTruePositives().size() + fEntity.getFalsePositives().size());
					if ((fEntity.getTruePositives().size()) + fEntity.getFalseNegatives().size() != 0)
					 recall = (double)(fEntity.getTruePositives().size()) / (fEntity.getTruePositives().size() + fEntity.getFalseNegatives().size());
					
//					System.out.println(String.valueOf(precision));
//					System.out.println(recall);
					arr.add(String.valueOf(precision));
					arr.add(String.valueOf(recall));
					
					data.put(String.valueOf(i+2), arr);
				}
				
				
				
	        ////////////////////////////////////////////////////////////////////////////////////////////////////////
	        //Iterate over data and write to sheet
	        Set<String> keyset = data.keySet();
	        int rownum = 0;
	        for (String key : keyset)
	        {
	            Row row = sheet.createRow(rownum++);
	            ArrayList<Object> objArr = data.get(key);
	            int cellnum = 0;
	            for (Object obj : objArr)
	            {
	               Cell cell = row.createCell(cellnum++);
	              cell.setCellStyle(cellStyle);
	               if(obj instanceof String)
	                    cell.setCellValue((String)obj);
	                else if(obj instanceof Integer)
	                    cell.setCellValue((Integer)obj);
	            }
	        }
	        try
	        {
	            //Write the workbook in file system
	        	File file = new File(InputParameters.exlEvalFilesFolder);
	        	file.mkdirs();
	            FileOutputStream out = new FileOutputStream(new File(InputParameters.exlEvalFilesFolder+"/"+InputParameters.exlMainEvaluationFileOutputName));
	            workbook.write(out);
	            out.close();
	        } 
	        catch (Exception e) 
	        {
	            e.printStackTrace();
	        }
	      
   }
	
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 public static void writeLabelEvaluationFile(ArrayList<FileEntity> fEntities) {
	     	
		   //Blank workbook
	        XSSFWorkbook workbook = new XSSFWorkbook(); 
	        CellStyle cellStyle = workbook.createCellStyle();
	        cellStyle.setAlignment(HorizontalAlignment.CENTER);
	        
	        //Create a blank sheet
	        XSSFSheet sheet = workbook.createSheet();
	          
	        //This data needs to be written (Object[])
	        Map<Integer, ArrayList<Object>> data = new TreeMap<Integer, ArrayList<Object>>();
	       
	        ArrayList<Object> titles = new ArrayList<>();
			titles.add("PMID");
			titles.add("Taxon ID");
			titles.add(LabelConstants.CATEGORY_METHOD+"(FP)");
			titles.add(LabelConstants.CATEGORY_RESULT+"(FP)");
			titles.add(LabelConstants.CATEGORY_FACT+"(FP)");
			titles.add(LabelConstants.CATEGORY_TITLE+"(FP)");
			titles.add(LabelConstants.CATEGORY_IMPLICATION+"(FP)");
			titles.add(LabelConstants.CATEGORY_GOAL+"(FP)");
			titles.add(LabelConstants.CATEGORY_PROBLEM+"(FP)");
			titles.add(LabelConstants.CATEGORY_HYPOTHESIS+"(FP)");
			titles.add(LabelConstants.CATEGORY_NONE+"(FP)");
			
			titles.add(LabelConstants.CATEGORY_METHOD+"(TP)");
			titles.add(LabelConstants.CATEGORY_RESULT+"(TP)");
			titles.add(LabelConstants.CATEGORY_FACT+"(TP)");
			titles.add(LabelConstants.CATEGORY_TITLE+"(TP)");
			titles.add(LabelConstants.CATEGORY_IMPLICATION+"(TP)");
			titles.add(LabelConstants.CATEGORY_GOAL+"(TP)");
			titles.add(LabelConstants.CATEGORY_PROBLEM+"(TP)");
			titles.add(LabelConstants.CATEGORY_HYPOTHESIS+"(TP)");
			titles.add(LabelConstants.CATEGORY_NONE+"(TP)");
			
			
			data.put(1, titles);
	        
			int rowIndex = 2;
			
				for (int i = 0; i < fEntities.size(); i++) {
					FileEntity fEntity = fEntities.get(i);
					ArrayList<Object> arr = new ArrayList<>();
					
					for (int j = 0; j < fEntity.getFalsePositives().size(); j++) {
						Entity en = fEntity.getFalsePositives().get(j);
						//System.out.println(en.getTaxonID());
						//System.out.println(rowIndex);
						arr.add(fEntity.getId());
						arr.add(en.getTaxonID());
						
						int count = LabelFilesReader.getCount(fEntity.getId(), en.getTaxonID(), LabelConstants.CATEGORY_METHOD);
						arr.add(count);
						count = LabelFilesReader.getCount(fEntity.getId(), en.getTaxonID(), LabelConstants.CATEGORY_RESULT);
						arr.add(count);
						count = LabelFilesReader.getCount(fEntity.getId(), en.getTaxonID(), LabelConstants.CATEGORY_FACT);
						arr.add(count);
						count = LabelFilesReader.getCount(fEntity.getId(), en.getTaxonID(), LabelConstants.CATEGORY_TITLE);
						arr.add(count);
						count = LabelFilesReader.getCount(fEntity.getId(), en.getTaxonID(), LabelConstants.CATEGORY_IMPLICATION);
						arr.add(count);
						count = LabelFilesReader.getCount(fEntity.getId(), en.getTaxonID(), LabelConstants.CATEGORY_GOAL);
						arr.add(count);
						count = LabelFilesReader.getCount(fEntity.getId(), en.getTaxonID(), LabelConstants.CATEGORY_PROBLEM);
						arr.add(count);
						count = LabelFilesReader.getCount(fEntity.getId(), en.getTaxonID(), LabelConstants.CATEGORY_HYPOTHESIS);
						arr.add(count);
						count = LabelFilesReader.getCount(fEntity.getId(), en.getTaxonID(), LabelConstants.CATEGORY_NONE);
						arr.add(count);
						
						arr.add(0);
						arr.add(0);
						arr.add(0);
						arr.add(0);
						arr.add(0);
						arr.add(0);
						arr.add(0);
						arr.add(0);
						arr.add(0);
						
						data.put(rowIndex, arr);
						rowIndex++;
						arr = new ArrayList<>();
						
					}
					
					
					
					for (int j = 0; j < fEntity.getTruePositives().size(); j++) {
						Entity en = fEntity.getTruePositives().get(j);
						arr.add(fEntity.getId());
						arr.add(en.getTaxonID());
						
						arr.add(0);
						arr.add(0);
						arr.add(0);
						arr.add(0);
						arr.add(0);
						arr.add(0);
						arr.add(0);
						arr.add(0);
						arr.add(0);
		
						int count = LabelFilesReader.getCount(fEntity.getId(), en.getTaxonID(), LabelConstants.CATEGORY_METHOD);
						arr.add(count);
						count = LabelFilesReader.getCount(fEntity.getId(), en.getTaxonID(), LabelConstants.CATEGORY_RESULT);
						arr.add(count);
						count = LabelFilesReader.getCount(fEntity.getId(), en.getTaxonID(), LabelConstants.CATEGORY_FACT);
						arr.add(count);
						count = LabelFilesReader.getCount(fEntity.getId(), en.getTaxonID(), LabelConstants.CATEGORY_TITLE);
						arr.add(count);
						count = LabelFilesReader.getCount(fEntity.getId(), en.getTaxonID(), LabelConstants.CATEGORY_IMPLICATION);
						arr.add(count);
						count = LabelFilesReader.getCount(fEntity.getId(), en.getTaxonID(), LabelConstants.CATEGORY_GOAL);
						arr.add(count);
						count = LabelFilesReader.getCount(fEntity.getId(), en.getTaxonID(), LabelConstants.CATEGORY_PROBLEM);
						arr.add(count);
						count = LabelFilesReader.getCount(fEntity.getId(), en.getTaxonID(), LabelConstants.CATEGORY_HYPOTHESIS);
						arr.add(count);
						count = LabelFilesReader.getCount(fEntity.getId(), en.getTaxonID(), LabelConstants.CATEGORY_NONE);
						arr.add(count);
						
						data.put(rowIndex, arr);
						rowIndex++;
						arr = new ArrayList<>();
						
					}
					
				
				}
				
				
				
	        ////////////////////////////////////////////////////////////////////////////////////////////////////////
	        //Iterate over data and write to sheet
	        Set<Integer> keyset = data.keySet();
	        List<Integer> keysetList = new ArrayList<Integer>(keyset); 
	        Collections.sort(keysetList); 
	        int rownum = 0;
	        for (Integer key : keysetList)
	        {
	            Row row = sheet.createRow(rownum++);
	            ArrayList<Object> objArr = data.get(key);
	            int cellnum = 0;
	            for (Object obj : objArr)
	            {
	               Cell cell = row.createCell(cellnum++);
	              cell.setCellStyle(cellStyle);
	               if(obj instanceof String) {
	                    cell.setCellValue((String)obj);
	                   // System.out.println("key:"+key+"   "+"cell:"+(String)obj);
	               }
	                else if(obj instanceof Integer) {
	                    cell.setCellValue((Integer)obj);
	                  //  System.out.println("key:"+key+"   "+"cell:"+(Integer)obj);
	                }
	            }
	        }
	        try
	        {
	            //Write the workbook in file system
	        	File file = new File(InputParameters.exlEvalFilesFolder);
	        	file.mkdirs();
	            FileOutputStream out = new FileOutputStream(new File(InputParameters.exlEvalFilesFolder+"/"+InputParameters.exlLabelEvaluationFileOutputName));
	            workbook.write(out);
	            out.close();
	        } 
	        catch (Exception e) 
	        {
	            e.printStackTrace();
	        }
	      
 }
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 public static void writeNumberReportFile(ArrayList<FileEntity> fEntities) {
	     	
		   //Blank workbook
	        XSSFWorkbook workbook = new XSSFWorkbook(); 
	        CellStyle cellStyle = workbook.createCellStyle();
	        cellStyle.setAlignment(HorizontalAlignment.CENTER);
	        
	        //Create a blank sheet
	        XSSFSheet sheet = workbook.createSheet();
	          
	        //This data needs to be written (Object[])
	        Map<String, ArrayList<Object>> data = new TreeMap<String, ArrayList<Object>>();
	       
	        ArrayList<Object> titles = new ArrayList<>();
			titles.add("PMID");
			titles.add("Total number of Pathogens");
			titles.add("Number of Repetitious");
		
			
			data.put("1", titles);
	        
				for (int i = 0; i < fEntities.size(); i++) {
					FileEntity fEntity = fEntities.get(i);
					ArrayList<Object> arr = new ArrayList<>();
					arr.add(fEntity.getId());
					
				
					
					int rep = 0;
					for (int j = 0; j < fEntity.getEntities().size(); j++) {
						Entity en = fEntity.getEntities().get(j);
							rep += (en.getLocations().size());
					}
					arr.add(rep);
				

					
					rep = 0;
					for (int j = 0; j < fEntity.getEntities().size(); j++) {
						Entity en = fEntity.getEntities().get(j);
						if (en.getLocations().size() > 1)
							rep += (en.getLocations().size() - 1);
					}
					arr.add(rep);
					
					
					data.put(String.valueOf(i+2), arr);
				}
				
				
				
	        ////////////////////////////////////////////////////////////////////////////////////////////////////////
	        //Iterate over data and write to sheet
	        Set<String> keyset = data.keySet();
	        int rownum = 0;
	        for (String key : keyset)
	        {
	            Row row = sheet.createRow(rownum++);
	            ArrayList<Object> objArr = data.get(key);
	            int cellnum = 0;
	            for (Object obj : objArr)
	            {
	               Cell cell = row.createCell(cellnum++);
	              cell.setCellStyle(cellStyle);
	               if(obj instanceof String)
	                    cell.setCellValue((String)obj);
	                else if(obj instanceof Integer)
	                    cell.setCellValue((Integer)obj);
	            }
	        }
	        try
	        {
	            //Write the workbook in file system
	        	File file = new File(InputParameters.exlEvalFilesFolder);
	        	file.mkdirs();
	            FileOutputStream out = new FileOutputStream(new File(InputParameters.exlEvalFilesFolder+"/"+InputParameters.exlNumberReportFileOutput));
	            workbook.write(out);
	            out.close();
	        } 
	        catch (Exception e) 
	        {
	            e.printStackTrace();
	        }
	      
 }
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
}