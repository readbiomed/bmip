import java.text.DecimalFormat;
import java.util.ArrayList;

public class PrintTotalPR {

	public static void print (ArrayList<FileEntity> fEntities) {
		double totalTruePositive = 0;
		double totalFalsePositive = 0;
		double totalFalseNegative = 0;
		for (int i = 0; i < fEntities.size(); i++) {
			totalTruePositive += fEntities.get(i).getTruePositives().size();
			totalFalsePositive += fEntities.get(i).getFalsePositives().size();
			totalFalseNegative += fEntities.get(i).getFalseNegatives().size();
		}
		double totalPrecision = (totalTruePositive/(totalTruePositive+totalFalsePositive)) * 100;
		double totalRecall = totalTruePositive/(totalTruePositive+totalFalseNegative) * 100;
	    DecimalFormat df2 = new DecimalFormat("#.##");
		System.out.println("Precision: " + df2.format(totalPrecision) + "%");
		System.out.println("Recall: " + df2.format(totalRecall) + "%");
	}
	
	
}
