

public class InputParameters {

	
	public static int searchMode = SearchMode.BY_LABEL;
	
	
	
	public static String OWLFileName = "ncbitaxon.owl";
	
	
	public static String XMLFileName = "results.xml";
	
	
	public static String CSVFileName = "MMandNCBI.txt";
	
	
	
	public static String StopWordsFileName = "stopwords.txt";
	
	
	
	public static String NotFoundItemsFileName = "sample1.txt";
	
}
